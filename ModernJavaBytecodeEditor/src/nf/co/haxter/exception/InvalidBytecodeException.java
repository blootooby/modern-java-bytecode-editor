package nf.co.haxter.exception;

// TODO better javadocs :L
/**
 * An exception that occurs when bytecode fails to be parsed.
 * 
 * @author blootooby
 */
public class InvalidBytecodeException extends Exception {
  private static final long serialVersionUID = 5221082800514571542L;

  /**
   * Creates an exception in which bytecode has failed to parse.
   * 
   * @param bytecode the bytecode that failed to parse.
   */
  public InvalidBytecodeException(String bytecode) {
    super("Invalid bytecode: " + bytecode);
  }

  /**
   * Creates an exception in which bytecode has failed to parse.
   * 
   * @param bytecodeArgs the arguments that failed to parse.
   */
  public InvalidBytecodeException(String[] bytecodeArgs) {
    this(combine(bytecodeArgs));
  }

  private static String combine(String[] s) {
    String r = "";
    for (String s1 : s) {
      r += s1;
    }
    return r;
  }
}
