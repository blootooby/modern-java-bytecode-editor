package nf.co.haxter.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.filechooser.FileFilter;

import nf.co.haxter.gui.tabs.ClassInfoPanel;
import nf.co.haxter.gui.tabs.FieldInfoPanel;
import nf.co.haxter.gui.tabs.MethodInfoPanel;
import nf.co.haxter.util.IOUtils;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class MainWindow {
  public static Color modifiedColor = Color.GRAY;
  private static FileFilter classFileFilter = new ClassFileFilter();
  public static final String nullString = "[null]";
  
  public static boolean CLASS_MODIFIED;

  public static MainWindow instance;

  private JFrame frmMjbe;

  private File openedFile;
  private ClassNode openedClass = new ClassNode();
  private JTabbedPane tabbedPane;

  private ClassInfoPanel classInfoPanel;
  private JPanel fieldInfoPanel;
  private JPanel methodInfoPanel;

  private JLabel statusBar;

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          MainWindow window = new MainWindow();
          window.frmMjbe.setVisible(true);
          instance = window;
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the application.
   */
  public MainWindow() {
    initialize();
  }

  /**
   * Initialize the contents of the frame.
   */
  private void initialize() {
    frmMjbe = new JFrame();
    frmMjbe.setTitle("MJBE");
    frmMjbe.setBounds(100, 100, 717, 489);
    frmMjbe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    tabbedPane = new JTabbedPane(JTabbedPane.TOP);
    frmMjbe.getContentPane().add(tabbedPane, BorderLayout.CENTER);

    JPanel panelStatusBar = new JPanel();
    panelStatusBar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
    frmMjbe.getContentPane().add(panelStatusBar, BorderLayout.SOUTH);
    panelStatusBar.setLayout(new BorderLayout(0, 0));

    statusBar = new JLabel(" ");
    statusBar.setHorizontalAlignment(SwingConstants.LEFT);
    panelStatusBar.add(statusBar, BorderLayout.WEST);

    JMenuBar menuBar = new JMenuBar();
    frmMjbe.setJMenuBar(menuBar);

    JMenu mnFile = new JMenu("File");
    menuBar.add(mnFile);

    JMenuItem mntmExit = new JMenuItem("Exit");
    mntmExit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        System.exit(0);
      }
    });

    JMenuItem mntmOpen = new JMenuItem("Open...");
    mntmOpen.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        // TODO file open dialog.
        JFileChooser fileChooser = new JFileChooser(".");
        fileChooser.setFileFilter(classFileFilter);
        int returnVal = fileChooser.showDialog(MainWindow.this.frmMjbe, "Select");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          MainWindow.this.openClassFile(fileChooser.getSelectedFile());
        }
      }
    });
    mnFile.add(mntmOpen);

    JMenuItem mntmCloseFile = new JMenuItem("Close file");
    mntmCloseFile.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MainWindow.this.closeFile();
      }
    });

    JMenuItem mntmSave = new JMenuItem("Save...");
    mntmSave.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent paramActionEvent) {
        MainWindow.this.saveFile();
      }
    });
    mnFile.add(mntmSave);
    mnFile.add(mntmCloseFile);
    mnFile.add(mntmExit);

    JMenu mnEdit = new JMenu("Edit");
    menuBar.add(mnEdit);

    JMenu mnView = new JMenu("View");
    menuBar.add(mnView);
  }

  public void openClassFile(File file) {
    this.openedFile = file;
    try {
      if (isLoadedFromJar()) {
        // TODO jar loading n shiz
      } else {
        ClassReader cr = new ClassReader(new FileInputStream(file));
        cr.accept(openedClass, 0);
      }

    } catch (Exception e) {
      e.printStackTrace();
      closeFile();
      JOptionPane.showMessageDialog(frmMjbe, "Failed to open file!");
      return;
    }
    this.removeTabs();
    this.initTabs();
  }

  public void initTabs() {
    JTabbedPane tabPane = this.getClassInfoTabbedPane();
    tabPane.addTab("Class Info", classInfoPanel = new ClassInfoPanel(this.openedClass));
    tabPane.addTab("Field Info", fieldInfoPanel = new FieldInfoPanel(this.openedClass));
    tabPane.addTab("Method Info", methodInfoPanel = new MethodInfoPanel(this.openedClass)); // TODO
  }

  public void removeTabs() {
    this.getClassInfoTabbedPane().removeAll();
  }

  public void closeFile() {
    this.openedFile = null;
    this.openedClass = new ClassNode();
    removeTabs();
  }

  public void saveFile() {
    // TODO
    this.classInfoPanel.saveClassProperties(this.openedClass);
    
    
    JFileChooser fc = new JFileChooser();
    int i = fc.showSaveDialog(frmMjbe);
    if (i == JFileChooser.APPROVE_OPTION) {
      ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
      this.openedClass.accept(cw);
      byte[] bytecode = cw.toByteArray();
      try {
        IOUtils.saveClassFileToDisk(bytecode, fc.getSelectedFile());
      } catch (IOException e) {
        e.printStackTrace();
        CLASS_MODIFIED = false;
        JOptionPane.showMessageDialog(frmMjbe, "Failed to save file!\n" + e.getMessage());
      }
    }
  }

  public boolean isLoadedFromJar() {
    return !this.openedFile.getName().endsWith(".class");
  }

  public JTabbedPane getClassInfoTabbedPane() {
    return tabbedPane;
  }

  public JLabel getStatusBar() {
    return statusBar;
  }

  public static void updateStatus(String status) {
    MainWindow.instance.statusBar.setText(status);
  }
}


class ClassFileFilter extends FileFilter {

  @Override
  public boolean accept(File arg0) {
    if (arg0.isDirectory()) {
      return true;
    }
    String ext = arg0.getName();
    int in = ext.lastIndexOf('.');
    if (in == -1) {
      return false;
    }
    ext = ext.substring(in);
    return ".jar".equals(ext) || ".class".equals(ext);
  }

  @Override
  public String getDescription() {
    return "Java classes (*.jar, *.class)";
  }

}
