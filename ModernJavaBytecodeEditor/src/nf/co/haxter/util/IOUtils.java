package nf.co.haxter.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

/**
 * A utility class for I/O operations.
 * 
 * @author blootooby
 */
public final class IOUtils {

  /**
   * Saves a class file to a JAR file.
   * 
   * @param bytecode the bytecode of the class file.
   * @param entryName the name of the class file.
   * @param jarFile the {@link File} of the JAR file.
   * @throws IOException If an I/O error occurs.
   * @see java.util.jar.JarFile
   */
  public static JarFile saveClassFileToOriginalJar(byte[] bytecode, String entryName, File jarFile)
      throws IOException {
    File tempJar = new File(jarFile.getParentFile(), "temp.jar");
    saveClassFileToModifiedJar(bytecode, entryName, new JarFile(jarFile), tempJar);
    if (!jarFile.delete()) {
      throw new IOException("Failed to delete file '" + jarFile.getAbsolutePath() + "'!");
    }
    if (!tempJar.renameTo(jarFile)) {
      throw new IOException("Failed to rename file '" + tempJar.getAbsolutePath() + "' to '"
          + jarFile.getAbsolutePath() + "'!");
    }
    return new JarFile(tempJar);
  }

  /**
   * Saves a class file to the hard disk.
   * 
   * @param bytecode the bytecode of the class file.
   * @param file the file on the disk to be saved to.
   * @throws IOException If an I/O error occurred.
   */
  public static void saveClassFileToDisk(byte[] bytecode, File file) throws IOException {
    ensureCreated(file);
    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream(file);
      fos.write(bytecode);
    } catch (IOException e) {
      if (fos != null) {
        fos.flush();
        fos.close();
      }
      throw e;
    } finally {
      if (fos != null) {
        fos.flush();
        fos.close();
      }
    }
  }

  /**
   * Saves a class file to a modified version of a JAR file.
   * 
   * @param bytecode the bytecode of the modified class file.
   * @param entryName the name of the modified class file.
   * @param jar the original JAR file.
   * @param modifiedJar the modified JAR file.
   * @throws IOException If an I/O error occurred.
   */
  public static void saveClassFileToModifiedJar(byte[] bytecode, String entryName, JarFile jar,
      File modifiedJar) throws IOException {
    JarOutputStream jos = null;
    ensureCreated(modifiedJar);
    try {
      jos = new JarOutputStream(new FileOutputStream(modifiedJar));
      Enumeration<JarEntry> entries = jar.entries();
      while (entries.hasMoreElements()) {
        JarEntry je = entries.nextElement();
        InputStream entryStream = jar.getInputStream(je);
        byte[] buffer = new byte[1024];
        int len = -1;
        jos.putNextEntry(je);
        if (entryName.equals(je.getName())) {
          jos.write(bytecode, 0, bytecode.length);
        } else {
          while ((len = entryStream.read(buffer)) != -1) {
            jos.write(buffer, 0, len);
          }
        }
        jos.closeEntry();
      }
    } finally {
      if (jos != null) {
        jos.flush();
        jos.close();
      }
    }
  }

  /**
   * A simple convenience method that creates a new file if it doesn't exist.
   * 
   * @param file the file to ensure.
   * @throws IOException If an I/O error occurred during the creation of the file.
   */
  private static void ensureCreated(File file) throws IOException {
    if (file != null) {
      if (!file.exists()) {
        file.getParentFile().mkdirs();
        file.createNewFile();
      }
    }
  }

  /**
   * Copies a file.
   * 
   * @param start the source file.
   * @param end the destination file.
   */
  public static void copyFile(File start, File end) throws IOException {
    FileInputStream fis = null;
    FileOutputStream fos = null;
    try {
      fis = new FileInputStream(start);
      fos = new FileOutputStream(end);
      int len = 0;
      byte[] buffer = new byte[1024];
      while ((len = fis.read(buffer)) != -1) {
        fos.write(buffer, 0, len);
      }
    } finally {
      if (fis != null) {
        fis.close();
      }
      if (fos != null) {
        fos.flush();
        fos.close();
      }
    }
  }
}
