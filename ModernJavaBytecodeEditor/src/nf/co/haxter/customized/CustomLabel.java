package nf.co.haxter.customized;

import java.lang.reflect.Field;

import org.objectweb.asm.Label;

/**
 * A {@link Label} with a custom offset.
 * @author blootooby
 */
public class CustomLabel extends Label {
  private int offset;
  
  /**
   * Creates a label with a custom offset.
   * @param offset the custom offset for this {@link Label}.
   */
  public CustomLabel(int offset) {
    this.offset = offset;
  }

  /**
   * Gets the offset of this label.
   */
  @Override
  public int getOffset() {
    return this.offset;
  }
  
  /**
   * Sets the offset of this label.
   * @param offset the new offset for this label.
   */
  public void setOffset(int offset) {
    this.offset = offset;
  }
}
